# Extraction
### An open-source, community-built battle royale game



If you would like to contribute, please join our [Discord server](https://discord.gg/vDkCqQX) to receive content updates and discuss codebase changes with fellow Extraction developers!

#### Extraction Resource Document and Project Guidelines
Before submitting new content and features to the project, please read the [Extraction Resource Document](https://docs.google.com/document/d/1rJwBNYJSHsEa01c_iTPzC5M8Ofzg9XUOgaSlMAb0SxI/edit?usp=sharing) to make sure your submissions adhere to the project!

#### Extraction Coding Standard
We have outlined a coding standard that all developers should follow when committing changes to the repository. You can find the document [here](https://docs.google.com/document/d/1hOHbVAf2MX7EtMs5JZM7beUVWC3VpTLTj8MvLLsTMiQ/edit?usp=sharing).