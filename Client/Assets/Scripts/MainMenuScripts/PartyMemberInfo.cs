﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;

public class PartyMemberInfo : MonoBehaviour {

    [SerializeField] Text username, userlevel;
    [SerializeField] Color defaultColor, readyColor;
    [SerializeField] Button kickBtn, readyBtn, profileBtn;
    public bool playerIsReady = false;
    public CSteamID steamid;

    public void SetUsername (string m_Username) {
        username.text = m_Username;
    }

    public void SetUserLevel (string m_UserLevel) {
        userlevel.text = "/" + m_UserLevel;
    }

    public void SetReadyState (bool m_ReadyState) {
        GetComponent<Image>().color = (m_ReadyState) ? readyColor : defaultColor;
    }

    public void ToggleReadyState() {
        playerIsReady = !playerIsReady;
        SetReadyState(playerIsReady);
    }

    public void ViewSteamProfile () {
        if (SteamManager.Initialized) {
            SteamFriends.ActivateGameOverlayToUser("steamid", steamid);
        }
    }

    public void SetButtonVisibility(string btn, bool vis) {
        switch (btn) {
            case "kick":
                kickBtn.enabled = vis;
                break;
            case "ready":
                readyBtn.enabled = vis;
                break;
            case "profile":
                profileBtn.enabled = vis;
                break;
        }
    }
}
