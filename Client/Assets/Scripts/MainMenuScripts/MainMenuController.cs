﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Steamworks;

public class MainMenuController : MonoBehaviour {

    private static MainMenuController instance;

    //----------------------------------------//
    // Menu Transitions                       //
    //----------------------------------------//
    [System.Serializable]
    public class MenuScreen {
        public string screenName;
        public GameObject screenObject;
        public bool isMain;
    }
    public MenuScreen[] menus;

    private void Start () {}

    public static MainMenuController GetInstance () {
        return instance;
    }

    public void QuitGame () {
        Application.Quit();
    }

    // Steam API Stuff
    public void ShowInviteScreen () {
        if (SteamManager.Initialized) {
            if (GameManager.GetInstance().steam_LobbyID != new CSteamID(0))
                SteamFriends.ActivateGameOverlayInviteDialog(GameManager.GetInstance().steam_LobbyID);
        }
    }

    public void ShowInventory () {
        if (SteamManager.Initialized) {
            
        }
    }
}