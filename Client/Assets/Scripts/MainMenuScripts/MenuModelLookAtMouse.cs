﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuModelLookAtMouse : MonoBehaviour {

    public Transform bone;
    public float speed = 1f;
    public LayerMask mask;

    private Vector3 target;
    private Quaternion origRot;
    private bool shouldLookAtTarget = false;

    private void Start () {
        origRot = bone.rotation;
    }

    void FixedUpdate () {

        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit mouseHit;
        if (Physics.Raycast(mouseRay, out mouseHit, Mathf.Infinity, ~mask)) {
            // Debug.Log("raycast ok");
            target = mouseHit.point;
            shouldLookAtTarget = true;
        }
        else
            shouldLookAtTarget = false;

        Vector3 lookDelta = (target - bone.position);
        Quaternion targetRot = Quaternion.LookRotation(lookDelta);

        bone.rotation = Quaternion.RotateTowards(bone.rotation, targetRot, speed);

        /*
        if (shouldLookAtTarget)
            // bone.LookAt(target);
            bone.rotation = Quaternion.RotateTowards(bone.rotation, Quaternion.Euler(targetRot), speed);
        else
            bone.rotation = origRot;
        */


        /*
        Plane plyPlane = new Plane(Vector3.up, bone.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float hitDist = 0f;

        if (plyPlane.Raycast(ray, out hitDist)) {
            Vector3 target = ray.GetPoint(hitDist);
            Quaternion rot = Quaternion.LookRotation(target - bone.position);
            bone.rotation = Quaternion.Slerp(bone.rotation, rot, speed * Time.fixedDeltaTime);
        }
        */

        // bone.LookAt(Camera.main.ScreenToWorldPoint(Input.mousePosition));
	}
}
