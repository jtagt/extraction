﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomModelPicker : MonoBehaviour {

    public GameObject[] models;

	// Use this for initialization
	void Start () {
		foreach (GameObject model in models) {
            model.SetActive(false);
        }
        models[Random.Range(0, models.Length - 1)].SetActive(true);
	}
}
