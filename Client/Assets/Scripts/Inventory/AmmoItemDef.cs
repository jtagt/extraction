﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoItemDef : UnityEngine.MonoBehaviour
{
    public enum AmmoType
    {
        Rifle,
        Pistol,
        Sniper,
        Shotgun,
        None
    }
    public AmmoType m_AmmoType = new AmmoType();
}
