﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemInfoBlockUI : MonoBehaviour {

    public Image itemImage;
    public Text itemName, itemType;
    public GameObject itemContextInfoPanel;

    private bool showItemContextMenu = false;

    // Use this for initialization
    void Start () {
        if (itemContextInfoPanel == null)
            itemContextInfoPanel = GameObject.FindGameObjectWithTag("_UI_ItemContextPanel");

        itemContextInfoPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update () {

        itemContextInfoPanel.SetActive(showItemContextMenu);

        RaycastHit mouseHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out mouseHit, Mathf.Infinity)) {
            if (mouseHit.transform.gameObject.tag == "_UI_ItemInfoBlock") {
                showItemContextMenu = true;
            }
        }

        if (showItemContextMenu)
            itemContextInfoPanel.transform.position = Input.mousePosition + new Vector3(0.1f, -0.1f);
    }

    public void SetName (string name) {
        itemName.text = name;
    }

    public void SetType (string type) {
        itemType.text = type;
    }

    public void SetImage (Sprite image) {
        itemImage.sprite = image;
    }
}
