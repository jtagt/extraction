﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public List<Item> m_Inventory = new List<Item>();
    // timer to stop players from using items right away
    // don't destroy items until  server confirms it was an honest pickup, just disable them on the local client.
    // this adds the item to the inventory and updates the UI.
    public void PickupItem(Item aItem, int aID )
    {
        if (m_Inventory.Contains(aItem))
        {
            if(aItem.itemDefinition.m_MaxStackSize > 1 && aItem.m_StackSize < aItem.itemDefinition.m_MaxStackSize)
            {
                // don't items need an currentStack variable?
                aItem.m_StackSize++;
            }
        }
        else
        {
            m_Inventory.Add(aItem);
        }
        // callbacks here to update UI
        RequestItumPickup(aItem.itemDefinition.m_ItemID, aID);
    }
    // this requests the pickup in an RPC, which will then in turn call Validate
    // ItemPickup, removing the item if it wasn't an honest Pickup.
    public void RequestItumPickup(int aItemID, int aID)
    {
        //RPC to request item pickup here; pass in item id (unique on a per item basis? Maybe pass in it's network ID.)
        // ExampleClient.GetInstance().clientNet.CallRPC("ValidateCast", UCNetwork.MessageReceiver.ServerOnly, -1, pos, rot, -1, 20f, "PickupItem");
    }

    /* will all items need a unique ID so the player knows which one to remove?
     Since we can't pass in the item object over an rpc, maybe store the item in a temp variable
     while it's being validated? Maybe use it's networkID. */
    public void ValidatePickup(bool valid, int aItemID)
    {
        bool removed = false;
        if (!valid)
        {
            // remove item here, UI callbacks as well
            foreach(Item aItem in m_Inventory)
            {
                if(aItem.itemDefinition.m_ItemID == aItemID)
                {
                    if (aItem.itemDefinition.m_MaxStackSize > 1 && aItem.m_StackSize > 1 && removed == false)
                    {
                        aItem.m_StackSize--;
                        removed = true;
                    }
                    else
                    {
                        m_Inventory.Remove(aItem);
                    }
                }
            }
        }
    }

    // drops the item and updates the UI, then calls the request function.
    public void DropItem(Item aItem)
    {
        bool removed = false;
        foreach (Item myItem in m_Inventory)
        {
            if (aItem == myItem)
            {
                if (aItem.itemDefinition.m_MaxStackSize > 1 && aItem.m_StackSize > 1 && removed == false)
                {
                    aItem.m_StackSize--;
                    removed = true;
                }
                else if(!removed)
                {
                    m_Inventory.Remove(aItem);
                    removed = true;
                }
            }
        }
        // gotta figure out how to deal with stacks and such before I do this part.
        RequestItemDrop(aItem.itemDefinition.m_ItemID);
    }
    /*  passes the itemID/NetworkID to the server with an RPC.
     server then verifies it has that item in it's authoritative inventory list for 
    this player. */
    public void RequestItemDrop(int aItemID)
    {
        // rpc to request that the server drops the item on the player.
         ExampleClient.GetInstance().clientNet.CallRPC("RequestItemDrop", UCNetwork.MessageReceiver.ServerOnly, -1, gameObject.GetComponent<PlayerController>().clientNetworkId, aItemID);
    }

    // if the drop isn't honest, it reverts the drop and UI changes.
    public void ValidateItemDrop(bool valid, int aItemID)
    {
        if (!valid)
        {

        }
    }
}
