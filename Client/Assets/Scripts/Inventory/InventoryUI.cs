﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour {

    private static InventoryUI instance;

    // Inventory UI Prefabs
    public GameObject inventoryItemListPrefab;

    [SerializeField] GameObject inventoryList;

    private void Awake () {
        if (instance == null)
            instance = this;
    }

    public static InventoryUI GetInstance() {
        return instance;
    }

    public void AddItemToInventory(Item itemToAdd) {
        GameObject uiItem = Instantiate(inventoryItemListPrefab, inventoryList.transform);
        uiItem.GetComponent<InventoryItemInfoBlockUI>().SetName(itemToAdd.itemDefinition.name);
        uiItem.GetComponent<InventoryItemInfoBlockUI>().SetType(itemToAdd.itemDefinition.GetType().ToString());
      //  uiItem.GetComponent<InventoryItemInfoBlockUI>().SetImage(itemToAdd.itemDefinition.itemImage);
    }
}
