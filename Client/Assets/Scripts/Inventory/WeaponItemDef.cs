﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeaponDefinition", menuName = "Items/Create Weapon Definition")]
public class WeaponItemDef : ItemDefinition
{
    public AmmoItemDef.AmmoType m_AmmoType = new AmmoItemDef.AmmoType();
    public int m_MagazineSize;
}
