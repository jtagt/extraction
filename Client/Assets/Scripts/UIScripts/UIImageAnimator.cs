﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIImageAnimator : MonoBehaviour {

    public enum AnimType {
        MultipleFrames,
        TransformRotate
    }
    public AnimType animType;

    public Sprite[] frames;
    public int currentFrame = 0;

    public bool useWaitTime = true;
    public float animWaitTime = 0.5f;
    private float waitTimeLeft;

    public float rotateSpeed = 1f;

	// Use this for initialization
	void Start () {
        waitTimeLeft = animWaitTime;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        switch (animType) {
            case AnimType.MultipleFrames:
                if (useWaitTime) {

                    waitTimeLeft -= Time.fixedDeltaTime;
                    if (waitTimeLeft <= 0f) {
                        if (currentFrame == frames.Length - 1)
                            currentFrame = 0;
                        else
                            currentFrame++;

                        GetComponent<Image>().sprite = frames[currentFrame];

                        waitTimeLeft = animWaitTime;
                    }

                }
                else {
                    if (currentFrame == frames.Length - 1)
                        currentFrame = 0;
                    else
                        currentFrame++;

                    GetComponent<Image>().sprite = frames[currentFrame];
                }
                break;
            case AnimType.TransformRotate:
                transform.Rotate(new Vector3(0, 0, rotateSpeed));
                break;
        }

	}
}
