﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour {

    [SerializeField] Text loadStatus;
    [SerializeField] Text loadingTip;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
    }

    public void SetLoadStatus (string status) {
        loadStatus.text = status;
    }

    public void SetTipText (string text) {
        loadingTip.text = text;
    }
}
