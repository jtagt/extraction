﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour {

    // Instance variables
    public CursorLockMode cursorLockMode = CursorLockMode.None; // The current state of the mouse pointer
    public bool hasWeapon = true; // Does the player currently have a weapon?
    public bool isRunning = false; // The player is currently running (holding run key)
    public bool isFreeLooking = false; // The player is currently free looking
    public bool isOnGround = false; // The player is currently on the ground
    public bool isCrouching = false; // The player is currently crouching
    public bool isAiming = false; // The player is currently aiming their weapon
    public bool isLookingAtItem = false; // The player is currently looking at an item
    public bool isAlive = true; // The player is currently alive (very important)

    // Movement
    public float walkSpeed = 6f; // The default movement speed of the player
    public float crouchSpeedMultiplier = 0.5f; // The speed of the player while crouching (walkSpeed * crouch)
    public float runSpeedMultiplier = 1.5f; // The speed of the player while running (walkSpeed * run)
    public float jumpForce = 1f; // How much force to apply to the player when jumping

    // Network
    public int clientNetworkId = -1; // The player's network client ID
    public bool playerEnabled = false; // If the player is currently able to be controlled
    public string playerName = "";

    // Rewired
    public int rewiredPlayerId = 0;
    private Player controller;
    private CharacterController playerCharacterController;
    // Player input variables
    public float strafeMovement, forwardMovement;
    public bool shouldFire = false;
    public bool shouldAim = false;
    public bool shouldCrouch = false;
    public bool shouldReload = false;
    public bool shouldJump = false;
    public bool shouldInteract = false;




    // Bullet origin
    public Transform bulletOrigin;

    // Active gun reference
    public GameObject activeWeapon;



    // Local, private variables for attached components and instances
    private GameManager gm;
    private ExampleClient gameClient;
    private Rigidbody rb;
    private Animator anim;
    private float camXRotation = 0f;



    // Model picker
    public GameObject[] models;
    public GameObject curModel;
    public int curModelIndex;



    // Function definitions

    // Initialize all private component variables
    private void Awake () {
        // Get the GameManager
        if (gm == null)
            gm = GameManager.GetInstance();

        // Get the networked client object
        if (gameClient == null)
            gameClient = ExampleClient.GetInstance();

        // Initialize Rewired components
        controller = ReInput.players.GetPlayer(rewiredPlayerId);
        if (playerCharacterController == null)
            playerCharacterController = GetComponent<CharacterController>();

        // Initialize rigidbody and animator component variables
        if (rb == null)
            rb = GetComponent<Rigidbody>();
        if (anim == null)
            anim = GetComponent<Animator>();

        // Set cursor lock state
        Cursor.lockState = cursorLockMode;
    }

    private void Start () {
        foreach (GameObject model in models)
            model.SetActive(false);

        curModelIndex = Random.Range(0, models.Length - 1);
        curModel = models[curModelIndex];
        curModel.SetActive(true);
    }

    private void Update () {

        // Update player ui
        PlayerUIController.GetInstance().ToggleAmmoUI(hasWeapon);

        GetPlayerInput();
        ProcessPlayerInput();

        //----------------------------------------//
        // Update player movement                 //
        //----------------------------------------//

        // Set the third person camera target to the player
        // Camera.main.transform.parent.GetComponent<TPCameraController>().playerTransform = gameObject.transform;

        isAlive = gm.playerAlive;
    }

    private void GetPlayerInput () {
        if (gm.playerAlive) {

            // Get movement input
            isRunning = controller.GetButton("Sprint"); // Is the player currently sprinting?

            strafeMovement = controller.GetAxis("LR"); // Get the current strafe movement from our controller (Left/Right)
            forwardMovement = controller.GetAxis("FB"); // Get the current forward movement from our controller (Front/Back)

            // Get button interactions
            shouldFire = controller.GetButtonDown("Shoot");
            shouldCrouch = controller.GetButtonDown("Crouch");
            shouldReload = controller.GetButtonDown("Reload");
            shouldJump = controller.GetButtonDown("Jump");
            shouldInteract = controller.GetButtonDown("GetItem");
        }
    }

    private void ProcessPlayerInput () {
        if (gm.playerAlive) {

            // Apply movement speeds and modifiers
            // Apply default walk speed. If the player is crouching then apply the crouch speed modifier
            // else if the player is running, apply the run speed modifier; if not then just walk at the default speed
            strafeMovement *= (walkSpeed * (isCrouching ? crouchSpeedMultiplier : isRunning ? runSpeedMultiplier : 1f));
            forwardMovement *= (walkSpeed * (isCrouching ? crouchSpeedMultiplier : isRunning ? runSpeedMultiplier : 1f));

            // Get the current mouse position and apply the sensitivity factor
            // Mouse sensitivity is controllable from options menu
            float mouseX = controller.GetAxis("HorizontalLook") * gm.mouseSensitivity;
            float mouseY = controller.GetAxis("VerticalLook") * gm.mouseSensitivity;

            // Set our current camera X rotation to the mouse position on the X-axis
            camXRotation += mouseX;

            // Do player movement via rigidbody
            Vector3 moveVec = new Vector3(strafeMovement, -2f, forwardMovement);
            // Get the rotation vector between the current rotation and where we're moving to
            Vector3 rotVec = transform.rotation * moveVec;
            // Set the velocity to our rotation vector
            rb.velocity = rotVec;

            // Update animation values with our current movement values
            anim.SetFloat("Forward", forwardMovement, 0.1f, Time.deltaTime);
            anim.SetFloat("Turn", strafeMovement, 0.1f, Time.deltaTime);

            // Rotate the player if we're not currently free-looking and not trying to play an animation
            // Also check to see if we're not in the inventory
            if (!controller.GetButton("FreeLook") && !controller.GetButton("OpenGestureMenu") && !controller.GetButton("OpenTauntMenu") && !gm.inventoryOpen) {
                // Rotate the player based on the mouse's x position
                transform.Rotate(0, mouseX, 0);
            }



            // Shooting
            if (shouldFire) {
                // If the player shoots their weapon, call an RPC to the server to request to fire
                Debug.Log("AttemptFire");

                // Play single-fire animation
                anim.SetTrigger("SingleFire");

                RaycastHit shotHit;
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out shotHit)) {
                    if (shotHit.collider.tag == "Player") {
                        Debug.Log("Calling RequestFire RPC with Victim");
                        gameClient.clientNet.CallRPC("RequestFire", UCNetwork.MessageReceiver.ServerOnly, -1,
                            Camera.main.transform.position, Camera.main.transform.forward, shotHit.collider.GetComponent<NetworkSync>().GetId(), GetComponent<NetworkSync>().GetId());
                    }
                    else {
                        Debug.Log("Calling RequestFire RPC without Victim");
                        gameClient.clientNet.CallRPC("RequestFire", UCNetwork.MessageReceiver.ServerOnly, -1, Camera.main.transform.position, Camera.main.transform.forward, -1, GetComponent<NetworkSync>().GetId());
                    }
                }
            }



            // Crouching
            anim.SetBool("Crouch", shouldCrouch);



            // Reloading
            if (shouldReload) {
                // Call an RPC to the server to reload the weapon
                gameClient.clientNet.CallRPC("RequestReload", UCNetwork.MessageReceiver.ServerOnly, -1);
            }



            // Aiming
            anim.SetBool("IsAiming", shouldAim);



            // Jumping
            if (isOnGround) {
                if (shouldJump)
                    rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }



            // Interacting
            if (shouldInteract) {
                RaycastHit itemLookHit;
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out itemLookHit, 10f) && itemLookHit.transform != transform) {
                    if (itemLookHit.collider.tag == "Item") {
                        isLookingAtItem = true;
                        if (controller.GetButtonDown("GetItem")) {
                            gameObject.GetComponent<PlayerInventory>().PickupItem(itemLookHit.collider.gameObject.GetComponent<Item>(), itemLookHit.collider.gameObject.GetComponent<NetworkSync>().GetId());
                        }
                    }
                    else { isLookingAtItem = false; }
                }
            }

        }
    }


    // Play animation states over the network
    public void PlayAnimationBool (string animName, bool animState) {
        anim.SetBool(animName, animState);
    }
    public void PlayAnimationFloat (string animName, float animState) {
        anim.SetFloat(animName, animState, 0.1f, Time.deltaTime);
    }
    public void PlayAnimationTrigger (string animName) {
        anim.SetTrigger(animName);
    }

    public void SetActivePlayerModel (int modelIndex) {
        foreach (GameObject model in models)
            model.SetActive(false);

        curModelIndex = modelIndex;
        curModel = models[curModelIndex];
        curModel.SetActive(true);
    }

}
