﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class Old_PlayerCon : MonoBehaviour
{

    [Header("Debug Settings")]
    private bool useUnifiedInput;
    public CursorLockMode cursorLock = CursorLockMode.None;
    [SerializeField] bool playerIsSprinting = false;
    [SerializeField] bool freeLookEnabled = false;
    [SerializeField] bool isOnGround = false;
    [SerializeField] bool isCrouching = false;
    private bool lookAtItem = false;
    // [SerializeField] Text debugText;

    [Header("Player Settings")]
    [SerializeField]
    float walkSpeed = 8f;
    [SerializeField] float crouchMultiplier = 0.5f;
    [SerializeField] float runMultiplier = 2f;
    [SerializeField] float jumpForce = 1f;
    [SerializeField] float playerHealth = 100f;

    public int networkId = -1;
    public bool playerEnabled = false;


    //rewired variables:
    public int playerID = 0;
    private Player player;
    private CharacterController charCon;

    /*
    [Header("Camera Settings")]
    [SerializeField] float camDist = -3.75f;
    [SerializeField] float camOffset = 1f;
    [SerializeField] float camHeight = 0.8f;
    [SerializeField] float damping = 5f;
    [SerializeField] bool smoothRotation = true;
    [SerializeField] float rotDamping = 10f;
    [SerializeField] float bumperDistCheck = 2.5f;
    [SerializeField] float bumperCamHeight = 1f;
    [SerializeField] Vector3 bumperOffset;
    */

    /*
    [Header("Camera Settings")]
    [SerializeField] GameObject cameraHolder;
    private float camVertRot = 0;
    */

    public enum WeaponType
    {
        None,
        Pistol,
        Rifle
    }
    [Header("Weapon Settings")]
    [SerializeField]
    WeaponType currentWeapon = WeaponType.None;
    [SerializeField] GameObject weapon;
    [SerializeField] float weaponDmg = 1f;
    [SerializeField] int curAmmo = 20;
    [SerializeField] int maxAmmo = 20;
    [SerializeField] int reserveAmmo = 200;
    enum FiringMode
    {
        Single,
        Burst,
        Auto
    }
    [SerializeField] FiringMode fireMode = FiringMode.Single;

    /*
    [Header("UI Settings")]
    [SerializeField] GameObject interactUIPanel;
    [SerializeField] Text interactUIText;
    [SerializeField] Image ui_PlayerHealth;
    [SerializeField] Text ui_CurrentAmmoText;
    [SerializeField] Text ui_ReserveAmmoText; // Also includes current firing mode
    [SerializeField] Text ui_ArmorRatingText;
    */




    // Private, inaccessible variables
    Rigidbody rb;
    Animator anim;

    private class PlayerInventory
    {

    }

    List<Item> inventory;

    float camXRot = 0;



    private void Awake()
    {
        // initializing Rewired components
        player = ReInput.players.GetPlayer(playerID);
        charCon = GetComponent<CharacterController>();

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        Cursor.lockState = cursorLock;

        inventory = new List<Item>();
    }

    private void Start () {
        // playerName.text = GameManager.GetInstance().localPlayerName;
    }

    private void Update()
    {

        useUnifiedInput = GameManager.GetInstance().useUnifiedInput;

        if (playerEnabled) {

            Camera.main.transform.parent.GetComponent<TPCameraController>().playerTransform = gameObject.transform;

            MoveAndRotate();

            GetInputActions();

            GroundCheck();
            Jump();

            ItemLookCheck();

            UpdateInfoUI();

            // CameraBumpCheck();

            // Update player animations depending on what weapon they have
            switch (currentWeapon) {
                case WeaponType.None:
                    anim.SetBool("HasRifle", false);
                    break;
                case WeaponType.Pistol:
                    anim.SetBool("HasRifle", false);
                    break;
                case WeaponType.Rifle:
                    anim.SetBool("HasRifle", true);
                    break;
            }

        }

    }








    // Does logic for player movement and rotation
    private void MoveAndRotate()
    {
        // Are we currently sprinting? Let's keep track of that
        playerIsSprinting = player.GetButton("Sprint");

        // Get the current movement input and add our movement factors to it


        float hMove = player.GetAxis("LR") * (walkSpeed * (isCrouching ? crouchMultiplier : playerIsSprinting ? runMultiplier : 1f));
        float vMove = player.GetAxis("FB") * (walkSpeed * (isCrouching ? crouchMultiplier : playerIsSprinting ? runMultiplier : 1f));


        // Get the current mouse position and apply our sensitivity factor to it
        float mouseXPos = player.GetAxis("HorizontalLook") * GameManager.GetInstance().mouseSensitivity;
        float mouseYPos = player.GetAxis("VerticalLook") * GameManager.GetInstance().mouseSensitivity;

        // Increase the camera's X rotation by the X position of the mouse
        camXRot += mouseXPos;

        //   Do Rigidbody velocity movement
        Vector3 moveVector = new Vector3(hMove, 0f, vMove);

        //    Get the rotation vector between the current rotation and where we're moving to
        Vector3 rotVector = transform.rotation * moveVector;

        //     Set the rigidbody's velocity to our rotation vector
        rb.velocity = rotVector;

        //    Update our animation values to play the correct movement animations
        anim.SetFloat("Forward", vMove, 0.1f, Time.deltaTime);
        anim.SetFloat("Turn", hMove, 0.1f, Time.deltaTime);

        // Rotate the player if we're not currently free-looking and not trying to play a gesture/taunt
        if (!player.GetButton("FreeLook") && (!player.GetButton("OpenGestureMenu") && !player.GetButton("OpenTauntMenu")) && !GameManager.GetInstance().inventoryOpen)
            transform.Rotate(0, mouseXPos, 0);

        // rb.AddForce(-transform.up, ForceMode.Force);
    }



    private void GetInputActions()
    {

        // Fire weapon
        if (player.GetButtonDown("Shoot"))
        {
            if (curAmmo > 0)
            {

                // anim.SetBool("FullAuto_b", true);

                switch (fireMode)
                {
                    case FiringMode.Single:
                        curAmmo--; break;
                    case FiringMode.Burst:
                        curAmmo -= 3; break;
                }

                RaycastHit hit;
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 20f) && hit.transform != transform)
                {
                    if (hit.collider.tag == "Target")
                    {
                        // hit.collider.gameObject.GetComponent<Target>().health -= (fireMode == FiringMode.Burst ? weaponDmg * 3 : weaponDmg);
                        // GameManager.GetInstance().DrawDebugText("Damaged " + hit.collider.gameObject.name + " for " + weaponDmg + " damage!");
                    }
                }

            }
            //RaycastHit hit;
            //if (Physics.Raycast(weapon.transform.position, weapon.transform.forward, out hit, 20f) && hit.transform != transform) {
            //    if (hit.collider.tag == "Target") {
            //        hit.collider.gameObject.GetComponent<Target>().health -= weaponDmg;
            //    }

            //}
        }



        // Reload
        if (Input.GetButtonDown("Reload") && reserveAmmo > 0 && curAmmo < maxAmmo)
        {
            if (reserveAmmo - maxAmmo < 0)
            {
                reserveAmmo = 0;
                curAmmo = reserveAmmo;
                //anim.SetBool("Reload_b", true);
            }
            else
            {
                int amountToGive = maxAmmo - curAmmo;
                curAmmo = maxAmmo;
                reserveAmmo -= amountToGive;
                //anim.SetBool("Reload_b", true);
            }

            // anim.SetBool("Reload", true);
        }

        // Crouching
        if (player.GetButton("Crouch"))
        {
            isCrouching = true;
            anim.SetBool("Crouch", true);
        }
        else
        {
            isCrouching = false;
            anim.SetBool("Crouch", false);
        }

        // anim.SetBool("FullAuto_b", false);

        anim.SetBool("IsAiming", player.GetButton("Aim"));
    }



    private void GroundCheck()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 1.1f))
        {
            isOnGround = true;
        }
        else
            isOnGround = false;
    }



    private void Jump()
    {
        if (isOnGround)
        {
            if (Input.GetButtonDown("Jump"))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }



    private void ItemLookCheck()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 10f) && hit.transform != transform)
        {
            if (hit.collider.tag == "Item")
            {
                lookAtItem = true;
                if (Input.GetButtonDown("Interact"))
                {
               //     hit.collider.gameObject.GetComponent<ItemManager>().itemDef.OnPickup();
                }
            }
            else
                lookAtItem = false;
        }

        // interactUIPanel.SetActive(lookAtItem);
    }



    private void UpdateInfoUI()
    {
        /*
        ui_PlayerHealth.rectTransform.rect.Set(
            ui_PlayerHealth.rectTransform.rect.x,
            ui_PlayerHealth.rectTransform.rect.y,
            playerHealth * 2,
            ui_PlayerHealth.rectTransform.rect.height
        );
        ui_CurrentAmmoText.text = curAmmo.ToString();
        ui_ReserveAmmoText.text = reserveAmmo.ToString() + " | " + fireMode.ToString();
        */
    }



    /*
    private void CameraBumpCheck() {

        Vector3 wantedPos = transform.TransformPoint(camOffset, camHeight, camDist);

        RaycastHit hit;
        Vector3 back = transform.TransformDirection(-1 * Vector3.forward);
        if (Physics.Raycast(transform.TransformPoint(bumperOffset), back, out hit, bumperDistCheck) && hit.transform != transform) {
            wantedPos.x = hit.point.x;
            wantedPos.z = hit.point.z;
            wantedPos.y = Mathf.Lerp(hit.point.y + bumperCamHeight, wantedPos.y, Time.deltaTime * damping);
        }

        Camera.main.transform.localPosition = Vector3.Lerp(Camera.main.transform.localPosition, wantedPos, Time.deltaTime * damping);
    }
    */



    public void PlayAnimationBool (string animName, bool runAnim) {
        anim.SetBool(animName, runAnim);
    }
    public void PlayAnimationFloat (string animName, float animFloat) {
        anim.SetFloat(animName, animFloat);
    }
    public void PlayAnimationTrigger (string animName) {
        anim.SetTrigger(animName);
    }





    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, (transform.position + new Vector3(0, -2, 0)));

        if (Input.GetButtonDown("Fire"))
        {
            Gizmos.DrawSphere(Camera.main.transform.position + new Vector3(0, 0, 10), 0.5f);
        }
    }

}
