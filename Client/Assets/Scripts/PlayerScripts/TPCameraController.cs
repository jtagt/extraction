﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class TPCameraController : MonoBehaviour
{

    public Transform playerTransform;
    public bool lerpTransform = false;
    public float cameraHeight, cameraDistance, cameraOffset;

    //rewired variables:
    public int playerID = 0;
    private Player player;
    private CharacterController charCon;


    private float camVertRot = 0f;

    private void Awake()
    {
        // initializing Rewired components
        player = ReInput.players.GetPlayer(playerID);
        charCon = GetComponent<CharacterController>();
    }

    private void Start()
    {
        transform.parent = playerTransform;
    }

    private void Update()
    {
        MoveCamera();
        CameraActions();
        UpdateReticle();
    }


    private void MoveCamera()
    {

        transform.localPosition = Vector3.zero;
        Camera.main.transform.localPosition = new Vector3(cameraOffset, cameraHeight, cameraDistance);

        float mouseXPos = player.GetAxis("HorizontalLook") * GameManager.GetInstance().mouseSensitivity;
        float mouseYPos = player.GetAxis("VerticalLook") * GameManager.GetInstance().mouseSensitivity;

        camVertRot -= mouseYPos;

        if (camVertRot < -50f)
            camVertRot = -50f;
        else if (camVertRot > 60f)
            camVertRot = 60f;

        if (!player.GetButton("GestureMenu") && !player.GetButton("TauntMenu") && !GameManager.GetInstance().inventoryOpen)
            if (player.GetButton("FreeLook"))
            {
                transform.Rotate(0f, mouseXPos, 0f, Space.Self);
            }
            else if (player.GetButtonDown("FreeLook"))
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                // transform.Rotate(camVertRot, 0, 0);
                transform.localRotation = Quaternion.Euler(camVertRot, 0, 0);
                // Camera.main.transform.localRotation = Quaternion.Euler(camVertRot, 0, 0);
                // Camera.main.transform.Rotate(0, mouseXPos, 0);
            }
    }

    private void CameraActions()
    {
        if (GameManager.GetInstance().useUnifiedInput)
        {
            //if (GamePad.GetButton(CButton.LS))
            //    Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 75, Time.deltaTime * 10f);
            //else if (GamePad.GetAxis(CAxis.LT) > 0f)
            //    Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 45, Time.deltaTime * 10f);
            //else
            //    Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 60, Time.deltaTime * 10f);
        }
        else
        {
            if (player.GetButton("Sprint"))
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 75, Time.deltaTime * 10f);
            else if (player.GetButton("Aim"))
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 45, Time.deltaTime * 10f);
            else
                Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 60, Time.deltaTime * 10f);
        }
    }

    private void UpdateReticle()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
        {
            if (hit.collider.tag == "Player")
            {
                PlayerUIController.GetInstance().SeeEnemy();
            }
            else
                PlayerUIController.GetInstance().ResetReticleColor();
        }
    }
}
