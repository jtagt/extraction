﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour {

    public GameObject muzzleFlashEffect;

	// Use this for initialization
	void Start () {
        muzzleFlashEffect.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (muzzleFlashEffect.activeSelf) {
            if (muzzleFlashEffect.GetComponent<ParticleSystem>().isStopped) {
                muzzleFlashEffect.SetActive(false);
            }
        }
	}

    public void DoFlashEffect() {
        muzzleFlashEffect.SetActive(true);
    }
}
