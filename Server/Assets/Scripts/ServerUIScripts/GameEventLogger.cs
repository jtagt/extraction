﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEventLogger : MonoBehaviour {

    private static GameEventLogger instance;

    public GameObject contentView;
    public GameObject eventTextPrefab;

    public enum EventType {
        ServerEvent,
        ChatEvent,
        GameEvent,
        ExitEvent
    }

    private void Awake() {
        if (instance == null)
            instance = this;
    }

    public static GameEventLogger GetInstance() { return instance; }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddEvent(string aEventMessage, EventType aEventType) {
        GameObject eventObject = Instantiate(eventTextPrefab, contentView.transform);
        string eventPrefix = "";
        switch (aEventType) {
            case EventType.ServerEvent: eventPrefix = "[>SRV] "; break;
            case EventType.ChatEvent: eventPrefix = "[Chat] "; break;
            case EventType.GameEvent: eventPrefix = "[Evnt] "; break;
            case EventType.ExitEvent: eventPrefix = "[EXIT] "; break;
        }
        eventObject.GetComponent<Text>().text = eventPrefix + aEventMessage;
        contentView.transform.parent.transform.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 0;
    }

    public void ClearEvents() {
        foreach (Transform eventObject in contentView.transform) {
            Destroy(eventObject.gameObject);
        }
    }
}
