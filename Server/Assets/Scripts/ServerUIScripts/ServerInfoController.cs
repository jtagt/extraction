﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerInfoController : MonoBehaviour {

    public static ServerInfoController instance;

    private ExampleServer srv;

    public Text externalIP, internalIP, gameState, numConnected, numAlive, serverUptime;

    private void Awake () {
        if (instance == null)
            instance = this;
    }

    private void Start () {
        srv = ExampleServer.instance;
    }

    void Update () {
        // set game state
        gameState.text = "Game State:            " + srv.gameState.ToString() + (srv.gameState == ExampleServer.GameState.PreGame ? " (" + Mathf.RoundToInt(srv.preGameWaitTimeLeft).ToString() + " seconds until GameActive)" : "");
        // set num connected
        numConnected.text = "# Connected Players:   " + srv.GetPlayerCount() + "/" + srv.maxPlayers.ToString();
        // set num alive
        numAlive.text = "# Players Alive:       " + srv.GetPlayerAliveCount() + "/" + srv.GetPlayerCount().ToString();
        // set server uptime
        serverUptime.text = "Server Uptime:         " + Mathf.RoundToInt(Time.realtimeSinceStartup).ToString();
    }
}
