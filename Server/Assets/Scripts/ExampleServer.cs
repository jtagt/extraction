﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System.Net;

public class ExampleServer : MonoBehaviour {

    class ItemDefenition {
        int m_ItemID;
        int m_StackSize;
    }

    public static ExampleServer instance;
    public ServerNetwork serverNet;
    public int portNumber = 9200;
    public int maxPlayers = 100;
    public GameEventLogger eventLog;
    public GameObject playerCollider;

    public string externalIP;
    public string internalIP;

    private bool firstStart = true;



    // Game State
    public enum GameState {
        PreGameWaitForPlayers = 1,
        PreGame = 2,
        GameActive = 3,
        GameEnd = 4
    }
    public GameState gameState = GameState.PreGameWaitForPlayers;
    public float preGameWaitTime = 60f;
    public float preGameWaitTimeLeft;

    public int clientTimeUpdateRate = 1;

    // Debug variables for hit detection
    Vector3 from, dir;
    List<Vector3> debugBulletImpacts = new List<Vector3>();

    // Stores a player
    class Player {
        public long clientId;
        public string playerName;
        public bool isReady;
        public bool isConnected;
        public bool isAlive;

        public int playerNetworkId;

        public bool hasWeaponEquipped = true;
        public bool isWeaponActive = true;
        public int currentAmmoCount = 30;
        public int reserveAmmoCount = 300;
        public int playerHealth = 100;

        public GameObject playerColliderObject;
    }
    List<Player> players = new List<Player>();

    public int GetPlayerCount () { return players.Count; }
    public int GetPlayerAliveCount () {
        int numAlive = 0;
        foreach (Player p in players)
            if (p.isAlive)
                numAlive += 1;

        return numAlive;
    }
    public int GetNumPlayersConnected () {
        int numConnected = 0;
        foreach (Player p in players) {
            if (p.isConnected)
                numConnected += 1;
        }
        return numConnected;
    }

    void Awake () {
        instance = this;

        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null) {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null) {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        if (eventLog == null) {
            eventLog = GameEventLogger.GetInstance();
        }

        //serverNet.EnableLogging("rpcLog.txt");
    }

    private void Start () {

        serverNet.serverController = this;

        eventLog.AddEvent("Server started!", GameEventLogger.EventType.ServerEvent);

        // Get external IP
        externalIP = new WebClient().DownloadString("http://ipv4.icanhazip.com").TrimEnd('\r', '\n');
        ServerInfoController.instance.externalIP.text = "External IP:           " + externalIP;
        eventLog.AddEvent("Retrieved external IP address!", GameEventLogger.EventType.ServerEvent);

        // Get internal IP
        foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList) {
            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                internalIP = ip.ToString();
                ServerInfoController.instance.internalIP.text = "Internal IP:           " + ip.ToString();
                eventLog.AddEvent("Retrieved internal IP address!", GameEventLogger.EventType.ServerEvent);
                break;
            }
        }

        preGameWaitTimeLeft = preGameWaitTime;
    }

    private void FixedUpdate () {
        if (gameState == GameState.PreGameWaitForPlayers)
            if (GetNumPlayersConnected() >= 2) {
                gameState = GameState.PreGame;
                serverNet.CallRPC("SetGameState", UCNetwork.MessageReceiver.AllClients, -1, (int)gameState);
            }

        // do pregame countdown
        if (gameState == GameState.PreGame && GetNumPlayersConnected() >= 2) {
            preGameWaitTimeLeft -= Time.fixedDeltaTime;
            // Debug.Log("pregame time left: " + preGameWaitTimeLeft.ToString());

            // Send updated wait time to all clients every clientTimeUpdateRate seconds
            if (Mathf.RoundToInt(preGameWaitTimeLeft) % clientTimeUpdateRate == 0) {
                // eventLog.AddEvent("Match starts in " + preGameWaitTimeLeft.ToString() + " seconds!", GameEventLogger.EventType.GameEvent);
                serverNet.CallRPC("SetPregameWaitTime", UCNetwork.MessageReceiver.AllClients, -1, Mathf.RoundToInt(preGameWaitTimeLeft));
            }

            if (preGameWaitTimeLeft <= 0f) {
                gameState = GameState.GameActive;
                eventLog.AddEvent("Game start!", GameEventLogger.EventType.GameEvent);
                serverNet.CallRPC("SetGameState", UCNetwork.MessageReceiver.AllClients, -1, (int)gameState);
                serverNet.CallRPC("GameStart", UCNetwork.MessageReceiver.AllClients, -1);
            }
        }
        else if (gameState == GameState.PreGame && GetNumPlayersConnected() < 2) {
            preGameWaitTimeLeft = preGameWaitTime;
            gameState = GameState.PreGameWaitForPlayers;
            serverNet.CallRPC("SetGameState", UCNetwork.MessageReceiver.AllClients, -1, (int)gameState);
        }

        else if (gameState == GameState.GameActive) {
            if (firstStart) {
                GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
                foreach (Player p in players) {
                    serverNet.CallRPC("RespawnPlayer", p.clientId, -1, spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)].transform.position);
                }
                firstStart = false;
            }
            else {
                int numAlive = 0;
                foreach (Player p in players) {
                    if (p.isAlive)
                        numAlive++;
                }
                if (numAlive == 1) {
                    gameState = GameState.GameEnd;
                }
            }
        }
        else if (gameState == GameState.GameEnd) {
            // kick all connected players
            foreach (Player p in players) {
                if (p.isConnected) {
                    serverNet.Kick(p.clientId);
                }
            }

            gameState = GameState.PreGameWaitForPlayers;
        }
    }

    private void Update () {
        if (Input.GetKeyDown(KeyCode.R)) {
            if (gameState == GameState.GameActive && GetNumPlayersConnected() >= 2) {
                preGameWaitTimeLeft = preGameWaitTime;
                firstStart = false;
                gameState = GameState.PreGame;
            }
            else if (gameState == GameState.GameActive && GetNumPlayersConnected() < 2) {
                preGameWaitTimeLeft = preGameWaitTime;
                firstStart = false;
                gameState = GameState.PreGameWaitForPlayers;
            }
            eventLog.AddEvent("Server restarted!", GameEventLogger.EventType.ServerEvent);
        }
    }

    // A client has just requested to connect to the server
    void ConnectionRequest (ServerNetwork.ConnectionRequestInfo data) {
        Debug.Log("Connection request from " + data.username);

        eventLog.AddEvent("Connection request from " + data.username, GameEventLogger.EventType.ServerEvent);

        // We either need to approve a connection or deny it
        if (players.Count < maxPlayers) {
            Player newPlayer = new Player();
            newPlayer.clientId = data.id;
            newPlayer.playerName = data.username;
            newPlayer.isConnected = false;
            newPlayer.isReady = false;
            newPlayer.isAlive = true;
            players.Add(newPlayer);

            serverNet.ConnectionApproved(data.id);
        }
        else {
            serverNet.ConnectionDenied(data.id);
        }
    }

    void OnClientConnected (long aClientId) {
        serverNet.CallRPC("SetNetworkID", aClientId, -1, aClientId);
        serverNet.CallRPC("SetGameState", aClientId, -1, (int)gameState);
        // Set the isConnected to true on the player
        foreach (Player p in players) {
            if (p.clientId == aClientId) {
                p.isConnected = true;
                eventLog.AddEvent(p.playerName + " connected!", GameEventLogger.EventType.ServerEvent);
                serverNet.CallRPC("SetPlayerName", UCNetwork.MessageReceiver.AllClients, -1, aClientId, p.playerName);
                if (gameState == GameState.PreGame) {
                    serverNet.CallRPC("SetPregameWaitTime", UCNetwork.MessageReceiver.AllClients, -1, preGameWaitTimeLeft);
                }
            }
        }

        /*
        serverNet.CallRPC("RPCTest", UCNetwork.MessageReceiver.AllClients, -1, 45);
        ServerNetwork.ClientData data = serverNet.GetClientData(serverNet.SendingClientId);
        serverNet.CallRPC("NewClientConnected", UCNetwork.MessageReceiver.AllClients, -1, aClientId, "bob");
        */
    }

    public void PlayerIsReady () {
        // Who called this RPC: serverNet.SendingClientId
        Debug.Log("Player is ready");

        // Set the isConnected to true on the player
        foreach (Player p in players) {
            if (p.clientId == serverNet.SendingClientId) {
                p.isReady = true;
            }
        }

        // Are all of the players ready?
        bool allPlayersReady = true;
        foreach (Player p in players) {
            if (!p.isReady) {
                allPlayersReady = false;
            }
        }
        if (allPlayersReady) {
            // serverNet.CallRPC("StartTurn", players[0].clientId, -1);
        }
    }

    void OnClientDisconnected (long aClientId) {
        // Set the isConnected to true on the player
        foreach (Player p in players) {
            if (p.clientId == aClientId) {
                p.isConnected = false;
                p.isReady = false;
                eventLog.AddEvent(p.playerName + " disconnected (Reason: Connection closed by client).", GameEventLogger.EventType.ExitEvent);
                players.Remove(p);
            }
        }
    }

    public void UpdatePlayerPositions (int networkId, Vector3 position) {
        foreach (Player p in players) {
            if (p.playerNetworkId == networkId) {
                p.playerColliderObject.transform.position = position;
                break;
            }
        }
    }



    /*
     * Remote Procedure Calls
     */

    public void Ping () {
        ServerNetwork.ClientData data = serverNet.GetClientData(serverNet.SendingClientId);
        serverNet.CallRPC("Pong", serverNet.SendingClientId, -1);
    }

    // DoCast RPC
    // Called from the client when a raycast check needs to be verified
    public bool DoCast (Vector3 clientPos, Vector3 clientRot, int clientHitNetId, float castDist) {
        bool castValid = false;

        if (clientHitNetId != -1) {
            Player attacker, victim = null;
            foreach (Player p in players) {
                if (p.clientId == serverNet.SendingClientId) {
                    attacker = p;
                }
                else if (p.playerNetworkId == clientHitNetId) {
                    victim = p;
                }
            }

            RaycastHit hit;
            GameObject victimCol = Instantiate(playerCollider, serverNet.GetNetObjById(clientHitNetId).position, Quaternion.identity);
            if (Physics.Raycast(clientPos, clientRot, out hit, castDist)) {
                if (hit.collider == victimCol)
                    castValid = true;
            }

            DestroyImmediate(victimCol);
        }
        else
            castValid = Physics.Raycast(clientPos, clientRot, castDist);

        return castValid;
    }

    public void ValidateCast (Vector3 clientPos, Vector3 clientRot, int clientHitNetId, float castDist, string callback) {
        if (DoCast(clientPos, clientRot, clientHitNetId, castDist))
            serverNet.CallRPC("CastValid", serverNet.SendingClientId, -1, callback);
        else
            serverNet.CallRPC("CastInvalid", serverNet.SendingClientId, -1, callback);
    }

    // RequestFire RPC
    // Called from the client when they attempt to fire their weapon
    // This function will check to see if it is a valid action (they have a weapon equipped and they have ammo)
    // as well as perform logic to determine what has been hit (and if the hit is valid as well)
    public void RequestFire (Vector3 attackerPosition, Vector3 attackerForward, int victimNetworkId, int attackerNetworkId) {

        // Keep track if the action is valid
        bool actionIsValid = false;
        // Also keep track of the player
        Player attacker = null;
        Player victim = null;

        if (attackerNetworkId <= 0) {
            Debug.LogWarning("[RequestFire] Attacker network id invalid, breaking out of function.");
            return;
        }

        foreach (Player p in players) {
            if (p.clientId == serverNet.SendingClientId) {
                attacker = p;
                Debug.LogWarning("[RequestFire] Set Attacker to " + attacker.playerName);
            }
            else if (p.playerNetworkId == victimNetworkId) {
                victim = p;
                Debug.LogWarning("[RequestFire] Set Victim to " + victim.playerName);
            }
        }

        // actionIsValid = attacker.hasWeaponEquipped && attacker.isWeaponActive && (attacker.currentAmmoCount > 0);
        if (attacker.isAlive && attacker.hasWeaponEquipped && attacker.isWeaponActive && attacker.currentAmmoCount > 0)
            actionIsValid = true;

        Debug.LogWarning("[RequestFire] ActionIsValid: " + actionIsValid.ToString());

        // if the action is not valid, then return
        if (!actionIsValid)
            return;

        // if the current state of the game is not the actual game, don't inflict any damage (but remove ammo obviously)
        if (gameState != GameState.GameActive) {
            return;
        }
        else {

            // ammo stuff
            Debug.LogWarning("[RequestFire] Only removing attacker ammo; GameState: " + gameState.ToString());
            Debug.LogWarning("[RequestFire] Attacker ammo before: " + attacker.currentAmmoCount.ToString());
            attacker.currentAmmoCount -= 1;
            Debug.LogWarning("[RequestFire] Attacker ammo after: " + attacker.currentAmmoCount.ToString());

            Debug.LogWarning("[RequestFire] Game Active, running raycast validation.");

            Debug.LogWarning("[RequestFire] Attempting to call `PlayGunshot` RPC on Attacker.");
            serverNet.CallRPC("PlayGunshot", UCNetwork.MessageReceiver.AllClients, -1, attackerNetworkId);
            Debug.LogWarning("[RequestFire] Called `PlayGunshot` RPC on Attacker.");

            Debug.LogWarning("[RequestFire] Attempting to call `SetPlayerAmmoCount` RPC on Attacker.");
            serverNet.CallRPC("SetPlayerAmmoCount", attacker.clientId, -1, attacker.currentAmmoCount, attacker.reserveAmmoCount);
            Debug.LogWarning("[RequestFire] Called `SetPlayerAmmoCount` RPC on Attacker.");

            // debug raycast and bullet hit (gizmos)
            // from = attackerPosition;
            // dir = attackerForward;

            // Spawn player colliders at the given positions
            if (victimNetworkId != -1) {
                Debug.LogWarning("[RequestFire] Victim NetID: " + victimNetworkId.ToString());
                RaycastHit atkHit;
                GameObject victimCollider = Instantiate(playerCollider, serverNet.GetNetObjById(victimNetworkId).position, Quaternion.identity);
                if (actionIsValid && Physics.Raycast(attackerPosition, attackerForward, out atkHit)) {
                    Debug.LogWarning("[RequestFire] Raycast check valid.");
                    if (atkHit.collider == victimCollider.GetComponent<Collider>()) {
                        Debug.LogWarning("[RequestFire] Shot hit valid at collider position " + victimCollider.transform.position.ToString());
                        Debug.LogWarning("[RequestFire] Victim health before: " + victim.playerHealth.ToString());
                        victim.playerHealth -= 10;
                        Debug.LogWarning("[RequestFire] Victim health after: " + victim.playerHealth.ToString());
                        if (victim.playerHealth <= 10) {
                            Debug.LogWarning("[RequestFire] Victim died; hp: " + victim.playerHealth.ToString());
                            victim.isAlive = false;
                            Debug.LogWarning("[RequestFire] Attempting to call `PlayerDeath` RPC on Victim.");
                            serverNet.CallRPC("PlayerDeath", victim.clientId, -1, attacker.playerName);
                            Debug.LogWarning("[RequestFire] Called `PlayerDeath` RPC on Victim.");
                            Debug.LogWarning("[RequestFire] Attempting to call `PlayerKilled` RPC on Attacker.");
                            serverNet.CallRPC("PlayerKilled", attacker.clientId, -1, victim.playerName);
                            Debug.LogWarning("[RequestFire] Called `PlayerKilled` RPC on Attacker.");
                            eventLog.AddEvent(string.Format("{0} was killed by {1}!", victim.playerName, attacker.playerName), GameEventLogger.EventType.GameEvent);
                        }
                        else {
                            Debug.LogWarning("[RequestFire] Attempting to call `SetPlayerHealth` RPC on Victim.");
                            serverNet.CallRPC("SetPlayerHealth", victim.clientId, -1, victim.playerHealth);
                            Debug.LogWarning("[RequestFire] Called `SetPlayerHealth` RPC on Victim.");
                        }
                    }

                    DestroyImmediate(victimCollider);

                    Debug.LogWarning("[RequestFire] Attempting to call `ShotHit` RPC on Attacker.");
                    serverNet.CallRPC("ShotHit", serverNet.SendingClientId, -1, atkHit.point, atkHit.normal);
                    Debug.LogWarning("[RequestFire] Called `ShotHit` RPC on Attacker.");
                }
            }
            else {
                Debug.LogWarning("[RequestFire] Victim ID not set, not returning `ShotHit` RPC.");
            }

        }
    }


    // Reload RPC
    public void RequestReload () {
        long clientId = serverNet.SendingClientId;
        Player p = null;
        foreach (Player ply in players) {
            if (ply.clientId == clientId) { p = ply; break; }
        }
        if (p != null) {
            if (p.hasWeaponEquipped && p.isWeaponActive) {
                if (p.reserveAmmoCount >= 30) {
                    int curAmmo = p.currentAmmoCount;
                    int requestedAmmo = 30 - p.currentAmmoCount;
                    if (p.reserveAmmoCount >= requestedAmmo) {
                        p.reserveAmmoCount -= requestedAmmo;
                        p.currentAmmoCount += requestedAmmo;
                        serverNet.CallRPC("DoReload", clientId, -1);
                        serverNet.CallRPC("SetPlayerAmmoCount", clientId, -1, p.currentAmmoCount, p.reserveAmmoCount);
                    }
                }
            }
        }
    }

    public void UpdatePlayerID (int playerNetworkId) {
        foreach (Player p in players) {
            if (p.clientId == serverNet.SendingClientId) {
                p.playerColliderObject = Instantiate(playerCollider);
                p.playerNetworkId = playerNetworkId;
            }
        }
    }

    public void OnDrawGizmos () {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(from + new Vector3(0, 0.9f, 0), new Vector3(0.35f, 1.85f, 0.35f));
        Gizmos.DrawRay(from, dir * 2);
        // Gizmos.DrawSphere(from, 0.5f);

        Gizmos.color = Color.red;
        foreach (Vector3 bulletImpact in debugBulletImpacts)
            Gizmos.DrawCube(bulletImpact, new Vector3(0.5f, 0.5f, 0.5f));
    }

    private void OnApplicationQuit () {

    }
}