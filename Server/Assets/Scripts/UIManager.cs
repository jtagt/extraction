﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    private static UIManager instance;

    [SerializeField] Text ipAddrPublic, ipAddrLocal, numConnected, numAlive, gameState;
    public string externalIP, internalIP;

    private void Awake () {
        if (instance == null)
            instance = this;
    }
    public static UIManager GetInstance() { return instance; }

    // Use this for initialization
    void Start () {
        // get public ip
        SetPublicIP(ExampleServer.instance.externalIP);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        UpdateNumConnected(ExampleServer.instance.GetPlayerCount().ToString());
        UpdateAlivePlayers(ExampleServer.instance.GetPlayerAliveCount().ToString());
	}

    public void SetPublicIP(string ipAddr) {
        ipAddrPublic.text = "Public IP: " + ipAddr;
        externalIP = ipAddr;
    }

    public void SetLocalIP(string ipAddr) {
        ipAddrLocal.text = "Local IP: " + ipAddr;
        internalIP = ipAddr;
    }

    public void UpdateNumConnected(string num) {
        numConnected.text = "Connected players: " + num;
    }

    public void UpdateAlivePlayers(string num) {
        numAlive.text = "Players alive: " + num;
    }

    public void UpdateGameState(string state) {
        gameState.text = "Game state: " + state;
    }
}
